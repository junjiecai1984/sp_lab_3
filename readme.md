## How to Run? 
To run the compiled executable (**basictats**), run these in the terminal

```cd <wd>``` where <wd> is the working directory containing the .c file
 
 Then in the terminal, run  
```./basicstats <file>```

\<file\> is the txt file path containing the data 



## Build from Source
To get the compiled executable from basicstats.c

Run these commands in the terminal
 
```cd <wd>``` where <wd> is the working directory containing the .c file

Then in the terminal, run
```gcc basicstats.c -o basicstats```





 