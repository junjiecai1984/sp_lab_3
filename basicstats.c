#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


// This is a program calculating the mean, sd, median value of a array,which values are loaded from a txt file.
// By Junjie Cai, 2020-04-29
/** =========================================
            "private" interval functions
 ========================================= **/
// validate and return file pointer
FILE *_open_file(char *fname) {
    FILE *file;
    file = fopen(fname, "r");
    if (file == NULL) {
        printf("Reading file failed!!");
        exit(0);
    } else {
        return file;
    }
}

// calculate mean
double _cal_mean(float *p, int n) {
    double sum = 0;
    for (int i = 0; i < n; i++)
        sum += p[i];
    return sum / n;
}

// calculate sd by formula sqrt((sum((xi - mean)^2))/N)
double _cal_sd(float *p, int n) {
    double mean = _cal_mean(p, n);

    double sd = 0;
    for (int i = 0; i < n; i++) {
        sd += pow(p[i] - mean, 2);
    }

    return sqrt(sd / n);
}

// calculate median. Assuming the array is sortred
double _cal_median(float *array, int n) {
    if (n % 2 == 1)
        return (array[n / 2]);
    else {
        double mid_a = array[n / 2 - 1];
        double mid_b = array[n / 2];
        return (mid_a + mid_b) / 2;
    }
}

//function to _sort_array array
void _swap(float *x, float *y) {
    float temp = *x;

    *x = *y;
    *y = temp;
}

// sort array in ascending order
void _sort_array(float *array, int n) {
    int i, j, min_idx;

    for (i = 0; i < n - 1; i++) {
        // found index of minimum number
        min_idx = i;
        for (j = i + 1; j < n; j++)
            if (array[j] < array[min_idx])
                min_idx = j;

        _swap(&array[min_idx], &array[i]);
    }
}


/** =========================================
                main function
 ========================================= **/

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Number of args is smaller than 2. Remember to pass file name...\n");
        return 0;
    }

    int n = 20;

    float *array = (float *) malloc(n * sizeof(float));

    char *fname = argv[1];
    FILE *fp = _open_file(fname);

    int array_length = 0;
    while (!feof(fp)) {
        fscanf(fp, "%f\n", (array + array_length));
        array_length++;

        // reach the maximum capacity of the array
        if (array_length == n) {
            // create a new array with twice the size of the the original one
            float *new_array = (float *) malloc(n * 2 * sizeof(float));
            // Copy data from the old array to new array
            memcpy(new_array, array, array_length * sizeof(float));
            // free memory of the original array
            free(array);

            array = new_array;
            n = n * 2;
        }
    }
    fclose(fp);
    double mean = _cal_mean(array, array_length);
    double sd = _cal_sd(array, array_length);
    _sort_array(array, array_length);
    double median = _cal_median(array, array_length);

    printf("Results:\n=================\n");
    printf("Number of array is: %d\n", array_length);
    printf("Mean value is: %f\n", mean);
    printf("Sd value is: %f\n", sd);
    printf("Median value is: %f\n", median);
    printf("Unused capacity: %d\n", n - array_length);
}